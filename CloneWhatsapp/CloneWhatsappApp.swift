//
//  CloneWhatsappApp.swift
//  CloneWhatsapp
//
//  Created by victor manzanero on 16/12/23.
//

import SwiftUI

@main
struct CloneWhatsappApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(ModelData())
        }
    }
}
