//
//  ContactsList.swift
//  CloneWhatsapp
//
//  Created by victor manzanero on 16/12/23.
//

import SwiftUI

struct ContactsList: View {
    @Environment(ModelData.self) var modelData
    
    @State var searchText: String = ""
    var body: some View {
        VStack {
            List {
                HStack {
                    Image(systemName: "archivebox.fill")
                    Text("Archivados")
                }
                ForEach (modelData.chats) { element in
                    ExtractedView(chat:  element)
                }
            }
            .searchable(text: $searchText, prompt: "Buscar")
            .navigationTitle("Chats")
        }
    }
}

#Preview {
    ContactsList()
        .environment(ModelData())
}

struct ExtractedView: View {
    
    var chat: ChatModel
    
    var body: some View {
        HStack {
            chat.image
                .resizable()
                .frame(width: 50, height: 50)
                .foregroundStyle(.gray)
                .clipShape(.circle)
            VStack {
                HStack {
                    Text(chat.name)
                        .multilineTextAlignment(.leading)
                        .offset(y: -10)
                        .font(.headline)
                        .bold()
                    Spacer()
                }
                .padding(.leading)
                HStack {
                    checkmarks(sent: chat.sent, received: chat.received, seen: chat.seen)
                    Text(chat.text)
                        .offset(y: -10)
                        .font(.subheadline)
                    Spacer()
                }
                .padding(.leading)
                
            }
            Spacer()
            VStack {
                Text(chat.time)
                    .font(.footnote)
                if chat.silenced {
                    Image(systemName: "bell.slash.fill")
                }
            }
            .offset(y: -10)
        }
    }
}

struct checkmarks: View {
    var sent: Bool
    var received: Bool
    var seen: Bool
    
    var body: some View {
        if sent && received && seen {
            ZStack {
                Image(systemName: "checkmark")
                Image(systemName: "checkmark")
                    .offset(x:4)
            }
            .foregroundStyle(Color.blue)
            .offset(y: -10)
        } else if sent && received && !seen {
            ZStack {
                Image(systemName: "checkmark")
                Image(systemName: "checkmark")
                    .offset(x:4)
            }
            .foregroundStyle(.gray)
            .offset(y: -10)
        } else if sent && !received && !seen {
            ZStack {
                Image(systemName: "checkmark")
            }
            .foregroundStyle(.gray)
            .offset(y: -10)
        }
    }
}
