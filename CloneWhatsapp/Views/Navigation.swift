//
//  ContentView.swift
//  CloneWhatsapp
//
//  Created by victor manzanero on 16/12/23.
//

import SwiftUI

struct ContentView: View {
    
    @State var search: String = ""
    
    var body: some View {
        NavigationStack {
            ContactsList()
                .toolbar {
                    ToolbarItemGroup(placement: .topBarLeading) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Image(systemName: "ellipsis.circle")
                                .tint(.black)
                        })
                    }
                    ToolbarItemGroup(placement: .topBarTrailing) {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Image(systemName: "camera.fill")
                                .tint(.black)
                        })
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Image(systemName: "plus.circle.fill")
                                .tint(.green)
                        })
                    }
            }
        }
    }
}

#Preview {
    ContentView()
        .environment(ModelData())
}
