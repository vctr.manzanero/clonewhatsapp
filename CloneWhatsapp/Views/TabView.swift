//
//  TabView.swift
//  CloneWhatsapp
//
//  Created by victor manzanero on 17/12/23.
//

import SwiftUI

struct TabViewList: View {
    var body: some View {
        TabView {
            Text("novedades")
                .tabItem {
                    VStack {
                        Image(systemName: "message")
                        Text("Novedades")
                    }
                }
            Text("Llamadas")
                .tabItem {
                    VStack {
                        Image(systemName: "phone")
                        Text("Llamadas")
                    }
                }
            Text("Comunidades")
                .tabItem {
                    VStack {
                        Image(systemName: "person.3")
                        Text("Comunidades")
                    }
                }
            ContentView()
                .tabItem {
                    VStack{
                        Image(systemName: "bubble.left.and.bubble.right.fill")
                            .background(.black)
                        Text("Chats")
                    }
                }
            Text("Perfil")
                .tabItem {
                    VStack {
                        Image(systemName: "person.crop.circle")
                        Text("Tu")
                    }
                }
        }
        .tint(.black)
    }
}

#Preview {
    TabViewList()
        .environment(ModelData())
}
