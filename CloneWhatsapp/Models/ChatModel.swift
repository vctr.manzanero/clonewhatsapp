//
//  ChatModel.swift
//  CloneWhatsapp
//
/*  Created by victor manzanero on 17/12/23.
"name": "Victor",
"image": "turtlerock",
"id": 100,
"text": "Hola como estas?",
"time": "10:25",
"sent": true,
"received": true,
"seen": false,
"silenced": false
*/

import Foundation
import SwiftUI

struct ChatModel: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var text: String
    var time: String
    var sent: Bool
    var received: Bool
    var seen: Bool
    var silenced: Bool
    
    private var imageName: String
    var image: Image {
        if imageName.elementsEqual("") {
            return Image(systemName: "person.crop.circle.fill")
        } else {
            return Image(imageName)
        }
    }
    
    
}
